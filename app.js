'use strict';

const app = require('./config/server');

var server = app.listen(6050, function() {
    console.log('Servidor Online na Porta 6050');
});

var io = require('socket.io').listen(server);

app.set('io', io);

app.config.socketio.configuraSocket(app);

// io.on('connection', function(socket) {
//     console.log('Qtde Terminais Ativos -> ', new Date(), ' - ', io.eio.clientsCount);
//     socket.on('disconnect', function() {
//         console.log('Qtde Terminais Ativos -> ', new Date(), ' - ', io.eio.clientsCount);
//         //socket.emit('usuariosLogados', { qtde_terminais: io.eio.clientsCount });
//         //socket.broadcast.emit('usuariosLogados', { qtde_terminais: io.eio.clientsCount });
//     });

//     // socket.emit('usuariosLogados', { qtde_terminais: io.eio.clientsCount });
//     // socket.broadcast.emit('usuariosLogados', { qtde_terminais: io.eio.clientsCount });

//     // socket.on('register', function() {
//     //     socket.emit('usuariosLogados', { qtde_terminais: io.eio.clientsCount });
//     //     socket.broadcast.emit('usuariosLogados', { qtde_terminais: io.eio.clientsCount });
//     // })
// });