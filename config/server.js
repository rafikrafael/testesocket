"use strict";

const express = require('express');
const bodyParser = require('body-parser');

var consign = require('consign');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));


// efetua o autoload das rotas, dos models e dos controllers para o objeto app
consign()
    .include('/config/socketio.js')
    .into(app);

module.exports = app;